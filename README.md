# custom

## vundle
```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```
## vim
```bash
wget -O ~/.vimrc https://gitlab.com/CristaLCorp/custom/-/raw/main/.vimrc
```
## tmux
```bash
wget -O ~/.tmux.conf https://gitlab.com/CristaLCorp/custom/-/raw/main/.tmux.conf
```
## zsh
```bash
HISTSIZE=100000

# works with the tmux plugin
ZSH_TMUX_AUTOSTART=false

# Fix Home/End key with ZSH/tmux. To know the code of a key, execute cat, press enter, press the key, then Ctrl+C.

bindkey '^[[H' beginning-of-line
bindkey '^[[F' end-of-line

# alias
# useful timing alias, feat didi la bringe
alias timemem="/usr/bin/time -f 'command %C\n%E\nMax Resident : %M'"
```
