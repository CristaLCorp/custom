" better safe than sorry
" https://stackoverflow.com/questions/5845557/in-a-vimrc-is-set-nocompatible-completely-useless
set nocompatible
filetype off " probably deprecated

" ----- PLUGINS -----
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Plugin list
" -> Python
" Plugin 'indentpython.vim'
" f7 : static syntax checker + style checker stops at the first style error ?
Plugin 'nvie/vim-flake8'
" get the ("ddddd <CR> dddd") indentation right
Plugin 'hynek/vim-python-pep8-indent'
" -> Completion
" Plugin 'valloric/youcompleteme'
" Plugin 'davidhalter/jedi-vim'
" Next search with colors
" -> Search
Plugin 'ctrlpvim/ctrlp.vim'
" -> NerdTree
Plugin 'preservim/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'ryanoasis/vim-devicons'
" -> General
" Get the absolute line numbers while in edit mode
Plugin 'ericbn/vim-relativize'
" Automatic quotes and parentheses closing
Plugin 'raimondi/delimitmate'
" -> Marks
"  Show marks in the gutter
Plugin 'kshenoy/vim-signature'
Plugin 'luochen1990/rainbow'
" Plugin 'frazrepo/vim-rainbow'
" -> Status Bar
Plugin 'itchyny/lightline.vim'
set laststatus=2
"let g:lightline = {
"      \ 'colorscheme': 'powerline',
"      \ }
" Show colors of hex values in all files
Plugin 'ap/vim-css-color'
" -> Themes
" Plugin 'gosukiwi/vim-atom-dark'
" Plugin 'joshdick/onedark.vim'
" Plugin 'dracula/vim'
" Plugin 'rakr/vim-one'
" Plugin 'mhartington/oceanic-next'
Plugin 'chriskempson/tomorrow-theme'
Plugin 'thedenisnikulin/vim-cyberpunk'
Plugin 'tomasr/molokai'
" -> surroundings : cs("
Plugin 'tpope/vim-surround'

call vundle#end()
" ----- GENERAL CONFIG -----
"  __THEMEs__
" > CyberPunk
set termguicolors
"colorscheme silverhand
colorscheme elflord
let g:one_allow_italics = 1 " I love italic for comments
" > OceanicNext
"autocmd ColorScheme * highlight Normal ctermbg=NONE guibg=NONE
"autocmd ColorScheme * highlight NonText ctermbg=NONE guibg=NONE
"autocmd ColorScheme * highlight Visual ctermbg=NONE guibg=NONE
set background=dark

" searching subdir
set path+=**
" display all matching tabs
set wildmenu


set nu                     " line number
set encoding=utf-8         " set UTF-8
"syntax on                  " syntax coloration
syntax enable
filetype plugin indent on  " required
let g:rainbow_active = 1   " Rainbow bracket settings (Plugin 'frazrepo/vim-rainbow')
set number relativenumber
nmap <F6> :NERDTreeToggle<CR>
" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" setting horizontal and vertical splits
" set splitbelow
" set splitright

"split navigations
" nnoremap <C-J> <C-W><C-J>
" nnoremap <C-K> <C-W><C-K>
" nnoremap <C-L> <C-W><C-L>
" nnoremap <C-H> <C-W><C-H>

" ----- TRAILING SPACEs and TABs -----
" hi SpecialKey ctermbg=red ctermfg=white
set listchars=tab:>-,trail:☺,extends:>,precedes:<,nbsp:%,eol:↲,space:.
nmap <F3> :set list!<CR>

" ----- FOLDING -----
" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar 
nnoremap <space> za

" note pour plus tard
" inoremap { {<CR>}<Esc>ko

" azerty remap
noremap ` '
noremap ' `
noremap . ;
noremap ; .

" ColorColumn the cool way
highlight ColorColumn ctermbg=15 ctermfg=1
call matchadd('ColorColumn', '\%81v', 100)

" ----- PYTHON SPECIAL -----
let python_highlight_all=1
" Debug
autocmd BufWinEnter *.py nmap <silent> <F5>:w<CR>:terminal python3 -m pdb '%:p'<CR>

" Setting up indendation
au BufNewFile,BufRead *.py set tabstop=4 softtabstop=4 shiftwidth=4 textwidth=79 expandtab autoindent smartindent fileformat=unix 
au BufEnter *.py colorscheme silverhand

" ----- YAML SPECIAL -----
au BufNewFile,BufRead *.yaml,*.yml set tabstop=2 softtabstop=2 shiftwidth=2 textwidth=79 expandtab autoindent smartindent fileformat=unix 
au BufEnter *.yaml,*.yml colorscheme cyberpunk
" ----- BASH SPECIAL -----
au BufNewFile,BufRead *.bash,*.sh set tabstop=4 softtabstop=4 shiftwidth=4 textwidth=79 expandtab autoindent smartindent fileformat=unix 
au BufEnter *.bash,*.sh colorscheme molokai
" Indent test


