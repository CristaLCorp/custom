#!/bin/bash

# remove previous installs
sudo apt-get remove docker
sudo apt-get remove docker-engine
sudo apt-get remove docker.io
sudo apt-get remove containerd
sudo apt-get remove runc

# set up repo
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg lsb-release

# add Docker official GPG key
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# add repo to list
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# install Docker
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# make it executable
sudo chmod +x /usr/local/bin/docker-compose
