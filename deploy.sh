#!/bin/sh

# ZSH
sudo apt install zsh vim curl -y

# VIM
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cp .vimrc ~/.vimrc
vim +PluginInstall +qall

# TMUX
cp .tmux.conf ~/.tmux.conf

# ZSH
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

## Fix MobaXterm Home/End key with ZSH
# bindkey '^[[1~' beginning-of-line
# bindkey '^[[4~' end-of-line

#plugins=(
#          zsh-autosuggestions
#          zsh-syntax-highlighting
#          colored-man-pages
#          history
#          catimg
#          git
#          encode64
#)
